<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function create(Request $req, $carType, $carPrice){
    	
        $carAge = $req['car_age'];
        $carName = $req['car_name'];
    

        return view('results', compact('carAge', 'carName', 'carPrice', 'carType'));
    }
}
