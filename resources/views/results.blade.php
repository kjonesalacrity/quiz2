@extends('layouts.master')

@section('body')
	<div id="inspage">
		<div class="container-fluid">
			<!--RESULT -->	
			<div class="row">
				<div class="col-lg-7 box">
					<h2>These are the values from the form</h2>
					<ul>
						<li>Car Age: {{ $carAge }}</li>
						<li>Car Name:{{ $carName }}</li>
						<li>Car Type:{{ $carType }}</li>
						<li>Car Price:{{ $carPrice }}</li>
					</ul>
				</div>
				<div class="col-lg-5 box">
					<h2>These are the answers for Parameters Questions</h2>
					<ol>
						<li>HTTP GET is shown in the url where HTTP POST is not. AND GET</li>
						<li>Depends on the data you want to send. If you are sending data to be processed it would be a post method. If you want to retrieve data then it would be a get method. Also dependant what type data you are sending across, if you are sending passwords and user information, then it would not be clever to use a get as then the data is seeable and easily retrived.</li>
						<li>POST</li>
						<li>method='GET' in the form tag</li>
					</ol> 
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3 box">
					<div class="row">
						<div class="col-lg-6 box smallbox"></div>
						<div class="col-lg-6 box smallbox"> </div>
					</div>
					<div class="row">
						<div class="col-lg-6 box smallbox"></div>
						<div class="col-lg-6 box smallbox"></div>	
					</div>
				</div>
				<div class="col-lg-9 box">
					<h2>These are the answers for CSS Questions</h2>
						
						<ul>
							<li>Cascading Style Sheet</li>
							<li>2. DOM: by class ('.'), 
										by id ('#'), 
										by tag ('h1')</li>
							<li>	
								<ul>
									<li>Class - When there are going to be mutile objects of (not all of the same tag name) that need the same styling. Such as when you want to make 3 divs have a white background and 3 divs have a red background, give each div a class. </li>
									<li>Id - When you are looking to stlye only one object that will have tha unqiue style. When you only want one div to be blue, just name that div a certain id to change it to blue.</li>
									<li>Tag - When you want all the tags to have the same style. Such as all the p paragraphs to have the same size of text and font.</li>
								</ul>
							</li>
							<li>view public/css/style</li>
							<li>view public/css/style</li>
							<li>Classifications with the name 'reg' who have a child with a paragraph tag will have text that is aligned to the right</li>
							<li>
								<ul>
									<li>This one is to say that either OR of class reg OR paragraph tag will have that CSS</li>
									<li>This one is to say that classification with the name of reg with the child of p will hace that CSS</li>
								</ul>
							</li>
							<li>view public/css/style</li>
							<li>Cards, Modal, Tooltips, Carosoel (not sure how to spell that, the thing that the buttons make it slide), alerts, tabs</li>
						</ul>
					 
				</div>
			</div>
		</div>
	</div>
@endsection