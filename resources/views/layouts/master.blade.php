<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initail-scale=1.0, shrink-to-fit=no">

		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
		<link rel="icon" href="{{ asset('images/beanLogo.png') }}">

		<script src="{{ asset('script/snap.svg-min.js') }}"></script>
		<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>	
		<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>

		<script type="text/javascript" src="{{ asset('js/Chart.min.js') }}"></script>

		<title>Jelly Beans</title>
	</head>

	<body>
		
		@section('body')
			Title
		@show
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	</body>
</html>
